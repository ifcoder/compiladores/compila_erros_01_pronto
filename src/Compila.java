import erros.ListaErros;
import parser.Parser;
import scanner.Scanner;

import java.io.FileReader;

public class Compila {
    public static void main(String[] args) throws Exception {
        FileReader in = new FileReader("teste01.txt");
        ListaErros listaErros = new ListaErros();

        //redefinimos o construtor de Scanner em scanner.flex, agora ele recebe uma listaErros vazia.
        Scanner scanner = new Scanner(in, listaErros);
        Parser parser = new Parser(scanner);

        parser.parse();
        if (!listaErros.hasErros()) {
            System.out.println("Sintaxe Correta");
        } else {
            System.out.println("Erros encontrados:");
            listaErros.dump();
        }
    }
}
